﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace array_manipulation
{
    class Program
    {
        static Dictionary<string, long> times = new Dictionary<string, long>();

        public static void Main(string[] args)
        {
            var sw = new System.Diagnostics.Stopwatch();
            var swParse = new System.Diagnostics.Stopwatch();
            var swOperating = new System.Diagnostics.Stopwatch();

            int n, m;

            long[] maxLines = new long[] { 1, 10, 100, 1000, 10000 };
            //long[] maxLines = new long[] { 1 };

            string outPath = @"C:\content\Dev\hackerrank\array-manipulation-large-out.txt";
            TextWriter oldOut = Console.Out;
            using (FileStream outfs = new FileStream(outPath, FileMode.Create))
            using (StreamWriter outsw = new StreamWriter(outfs))
            {
                Console.WriteLine(outPath);
                Console.SetOut(outsw);

                foreach (long ml in maxLines)
                {
                    Console.WriteLine("***");
                    const int BufferSize = 128;
                    using (var fileStream = File.OpenRead(@"C:\content\Dev\hackerrank\array-manipulation-large-input.txt"))
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
                    {
                        string line;
                        line = streamReader.ReadLine();
                        string[] _l = line.Split(' ');
                        n = int.Parse(_l[0]);
                        m = int.Parse(_l[1]);

                        long[] list = new long[n];
                        sw.Start();
                        for (int i = 0; i < n; i++) list[i] = 0;
                        sw.Stop();
                        times["Initializing list"] = sw.ElapsedMilliseconds;

                        long swOperatingPrev = 0;
                        decimal avgRatio = 1;
                        int linesCount = 0;
                        for (int lcount = 0; lcount < m; lcount++)
                        {
                            swParse.Start();
                            line = streamReader.ReadLine();
                            _l = line.Split(' ');
                            int a = int.Parse(_l[0]);
                            int b = int.Parse(_l[1]);
                            long k = long.Parse(_l[2]);
                            swParse.Stop();
                            //Console.WriteLine("{0} {1} {2}", a, b, k);

                            int posDelta = b - a;
                            //Console.Write("b-a: {0}", posDelta);
                            swOperating.Start();
                            for (int i = a - 1; i < b - 1; i++)
                            {
                                list[i] = list[i] + k;
                            }
                            swOperating.Stop();
                            long elapsedThis = swOperating.ElapsedMilliseconds - swOperatingPrev;
                            decimal ratio = decimal.Round((decimal)elapsedThis * 100000 / posDelta, 2);
                            avgRatio = (avgRatio + ratio) / 2;
                            //Console.WriteLine(", time: "+elapsedThis+", ratio: "+ratio);
                            //Console.Write(".");
                            swOperatingPrev = swOperating.ElapsedMilliseconds;

                            if (lcount >= ml) break;
                            linesCount++;
                        }
                        //Console.WriteLine();
                        Console.WriteLine("lines: {0}", linesCount);
                        times["Operating"] = swOperating.ElapsedMilliseconds;
                        Console.WriteLine("avgRatio: {0}", decimal.Round(avgRatio, 2));
                    }
                    times["Parsing"] = swParse.ElapsedMilliseconds;

                    foreach (var k in times.Keys)
                        Console.WriteLine("total time '" + k + "': " + times[k]);
                    Console.WriteLine();
                }
            }
            Console.SetOut(oldOut);
            Console.WriteLine("end");
            //Console.Read();
        }
    }
}
