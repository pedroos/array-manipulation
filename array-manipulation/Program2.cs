﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace array_manipulation
{
    class Program2
    {
        public static void Main(string[] args)
        {
            const int BufferSize = 128;
            using (var fileStream = File.OpenRead(@"C:\content\Dev\hackerrank\array-manipulation-sample-input.txt"))
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8, true, BufferSize))
            {
                string[] _l = streamReader.ReadLine().Split(' ');
                int listSize = int.Parse(_l[0]);
                int operationCount = int.Parse(_l[1]);

                // first key is the initial position. second key is the ending position. the tuple is the value in the range and 
                // the initial and final positions of other same-value ranges generated from the same operation.
                var ranges = new SortedDictionary<int, SortedDictionary<int, Tuple<long, int, int>>>();

                for (int o = 0; o < operationCount; o ++)
                {
                    _l = streamReader.ReadLine().Split(' ');
                    int a = int.Parse(_l[0]);
                    int b = int.Parse(_l[1]);
                    long k = long.Parse(_l[2]);

                    if (!ranges.ContainsKey(a))
                    {
                        ranges.Add(a, new SortedDictionary<int, Tuple<long, int, int>>() {
                            { b, new Tuple<long, int, int>(k, -1, -1) }
                        });
                        continue;
                    }
                    if (!ranges[a].ContainsKey(b))
                    {
                        ranges[a].Add(b, new Tuple<long, int, int>(k, -1, -1));
                        continue;
                    }
                    ranges[a][b] = new Tuple<long, int, int>(ranges[a][b].Item1 + k, -1, -1);
                }

                Console.WriteLine("End, ranges is:");
                Console.WriteLine(string.Join(Environment.NewLine, ranges.Select(r0 => string.Join(Environment.NewLine, 
                    r0.Value.Select(r1 => r0.Key.ToString().PadLeft(8) + " " + r1.Key.ToString().PadLeft(8) + " " + 
                    r1.Value.Item1.ToString().PadLeft(10) + " " + r1.Value.Item2.ToString().PadLeft(8) + " " + 
                    r1.Value.Item3.ToString().PadLeft(8)).ToList())).ToList()));
            }
            Console.Read();
        }
    }
}
